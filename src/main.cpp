#include <Arduino.h>
#include <ESP8266WiFi.h>

//const char* ssid = "Net-Virtua-6929";
//const char* password = "20369290";
const char* ssid = "DESKTOP-I5CL9A4B";
const char* password = "autismomodeon";

  
    //PONTE H
    int m1s1 = 5; //Ligado ao pino 2 do L293D
    int m1s2 = 4; //Ligado ao pino 7 do L293D
    int m2s1 = 14;
    int m2s2 = 12;

    //LDR
    int pinopot = A0;  //Pino ligado ao LDR 
    int valorpot = 0; //Armazena valor lido do LDR, entre 0 e 1023 
    float luminosidade = 0; //Valor de luminosidade do led

WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);

    //PINs
  pinMode(pinopot, INPUT);  //Define o pino do LDR como entrada  
  pinMode(m1s1, OUTPUT);
  pinMode(m1s2, OUTPUT);
  pinMode(m2s1, OUTPUT);
  pinMode(m2s2, OUTPUT);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {
  
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Wait until the client sends some data
  Serial.println("new client");
  while (!client.available()) {
    delay(1);
  }

  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  // Match the request
  if (req.indexOf("down") != -1) {
       
         digitalWrite(m1s1, HIGH);
         digitalWrite(m2s2, HIGH);
         delay(1000);
         digitalWrite(m1s1, LOW);
         digitalWrite(m2s2, LOW);
  }
  if (req.indexOf("up") != -1) {
      
         digitalWrite(m1s2, HIGH);
         digitalWrite(m2s1, HIGH);
         delay(1000);
         digitalWrite(m1s2, LOW);
         digitalWrite(m2s1, LOW);
  }
   if (req.indexOf("left") != -1) {
       
         digitalWrite(m1s1, HIGH);
         digitalWrite(m2s1, HIGH);
         delay(1000);
         digitalWrite(m1s1, LOW);
         digitalWrite(m2s1, LOW);
    
  }
  if (req.indexOf("right") != -1) {
      
         digitalWrite(m1s2, HIGH);
         digitalWrite(m2s2, HIGH);
         delay(1000);
         digitalWrite(m1s2, LOW);
         digitalWrite(m2s2, LOW);
    
  }// else if (req.indexOf("/gpio/1") != -1) {
   // val = 1;


  client.flush();

  // Prepare the response
  String html = "<!DOCTYPE html>";
  html += "<html>";
  html += "<head>";
  html += " <meta charset=\"UTF-8\">";
  html += "<meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\">";
  html += "<meta http-equiv=\"Refresh\" content=\"5\">";
  html += "<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">";
  html += "<link rel=\"stylesheet\" href=\"https://code.getmdl.io/1.3.0/material.indigo-pink.min.css\">";
  html += "<link rel=\"stylesheet\" href=\"https://code.getmdl.io/1.3.0/material.blue_grey-indigo.min.css\"/>";
  html += "<script defer src=\"https://code.getmdl.io/1.3.0/material.min.js\"></script>";
  html += "<style>";
         if (luminosidade <= 200){
  html += "body{ background-color:white;}";
  }
    else{
      html += "body{";
      html += "background-image: url(\"https://media.giphy.com/media/3ohzdFHDBEG32PmWJO/giphy.gif\");"; 
      html +="background-repeat: no-repeat; ";
      html +="background-position: 50% ;";
      html +="margin-top:-50%;}";
  }
  html += "#wrapper{";
  html += "   height: 10em;";
  html += "   position: relative; ";
  html += "}";
  html += "#controls{";
  html += "   margin: 0;";
  html += "   position: absolute;";
  html += "   top: 50%;";
  html += "   left: 50%;";
  html += "   margin-right: -50%;";
  html += "   transform: translate(-50%, -50%)";
  html += "}";
  html += ".luminosidade{";
  html += "   font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;";
  html += "   font-size: 25px;";
  html += "   margin: 0;";
  html += "   position: absolute;";
  html += "   top: 50%;";
  html += "   left: 50%;";
  html += "   margin-right: -50%;";
  html += "   transform: translate(-50%, -50%)";
  html+= "}";
  html += "</style>";
  html += "</head>";
  html += "<title> Control </title>";
  html += "<body>"; 
  html += "<div id=\"wrapper\">";
  html += "<div id=\"controls\">";
  html += "<a href=\"left\"<button id=\"esquerda\" class=\"mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored\"> <i class=\"material-icons\"> arrow_back </i> </button></a>";
  html += "<a href=\"down\"<button id=\"baixo\"  class=\"mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored\"> <i class=\"material-icons\">arrow_downward</i> </button></a>";
  html += "<a href=\"up\"<button id=\"cima\"  class=\"mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored\"><i class=\"material-icons\">arrow_upward </i></button></a>";
  html += "<a href=\"right\"<button id=\"direita\" class=\"mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored\"><i class=\"material-icons\">arrow_forward</i></button></a>";
  html += "</div>";
  html += "</div>";
  html += "<div class=\"luminosidade\"><i class=\"material-icons\"> brightness_5 </i> Luminosidade :" + String(luminosidade) + "</div>";
  html += "</body>"; 
  html += "</html>";

  // Send the response to the client
  client.print(html);
  delay(1);
  Serial.println("Client disonnected");

  // The client will actually be disconnected
  // when the function returns and 'client' object is detroyed
        // Le o valor - analogico - do LDR  
        valorpot = analogRead(pinopot);  
         // Converte o valor lido do LDR
        luminosidade = map(valorpot, 0, 1023, 0, 255); 
         delay(2000);      
}